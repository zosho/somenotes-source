(use-modules (haunt asset)
	     (haunt utils)
             (haunt site)
	     (haunt post)
	     (haunt html)
	     (haunt page)
             (haunt builder blog)
             (haunt builder atom)
	     (haunt builder rss)
	     (haunt builder assets)
	     (haunt builder flat-pages)
             (haunt reader commonmark)
	     (srfi srfi-19)
	     (web uri))

(define (current-reading) `(em "James-Percival Everett, A Biography of the Pixel-Alvy Ray Smith"))
(define (previous-reading) `(em "Dandelion Wine-Ray Bradbury, Tyger-S F Said, Fractals, on the edge of chaos-Oliver Linton, Kofi and the Rap Battle Summer–Jeffrey Boakye, God's Children Are Little Broken Things–Arinze Ifeakandu"))
(define (current-project) `(em "1/ BBC Radio channels RSS"))
(define (current-listening) `(em "kuniko plays reich–Kato Kuniko"))
  
(define (stylesheet name)
  `(link (@ (rel "stylesheet")
            (href ,(string-append "/css/" name ".css")))))


(define (anchor content uri)
  `(a (@ (href ,uri)) ,content))


					;(define (haunt-post-template post)
;  `(section ((h2 ,(post-ref post 'title))
;    (h3  ,(date->string* (post-date post)))
;    (div ,(post-sxml post)))))

(define haunt-theme
  (theme #:name "Haunt"
         #:layout
         (lambda (site title body)
           `((doctype "html")
             (head
              (meta (@ (charset "utf-8")))
	      (link (@ (rel "alternate")
		       (type "application/rss+xml")
		       (title "Somenotes")
		       (href "https://somenotes.stevelloyd.net/feed.xml")))
	      (link (@ (rel "alternate")
		       (type "application/atom+xml")
		       (title "Somenotes")
		       (href "https://somenotes.stevelloyd.net/atom.xml")))


	      (title ,(string-append title " — " (site-title site)))
		       ,(stylesheet "pico")
		       ,(stylesheet "custom")
	      (body
	       (header (@ (class "container"))
		       (nav (ul
			     (li (h3 ,(site-title site)))
			     (li (h3 ,(anchor "home" "/")))
			     (li (h3 ,(anchor "about" "/about.html")))
			     (li (h3 ,(anchor "now" "/new-now.html"))))))			     
	       
					(div (@ (class "container"))

					     ,body
(hr 
 (footer (@ (class "text-center"))
	 (a (@
	     (rel "me")
	     (href "https://toot.wales/@zosho")
	     (class "hide"))"Mastodon")
			    (p "Copyright © 2024 Stephen Lloyd. Content is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License. Built with " (em ,(anchor "haunt." "https://dthompson.us/projects/haunt.html")) )
			    ,(image "CC-88x31.png" "") )))))))
		   	          #:post-template
         (lambda (post)
           `((h3 ,(string-append (post-ref post 'title) " — " (date->string* (post-date post))))
             (div ,(post-sxml post))))))



(define (image src alt)
  `(p (img
    (@ (src ,(string-append "/images/" src))))))
       


(define (about-page site posts)
  (define body
    
    `(section (@ (class "container"))
	      ((h2 "Hello.")
	       (figure (img (@ (src "/images/about-palette-obuse.png")
			       (alt "A painter’s palette from Obuse, Hokusai’s hometown, Japan.")))
		       (figcaption "A painter’s palette from Obuse, Hokusai’s hometown, Japan"))
	       (p "I'm an engineer with experience in embedded systems, publishing and STEAM education. I'm happiest when building in Lisps as they seem to represent a concise and enjoyable way of representing knowledge so that it can be understood by people and processed by machines. (I'm with Hal Abelson and the Sussmans on this.)")
	       (p "I like taking walks and photos and also reading.")
	       (p "Most of my social media stuff is now on " ,(anchor "Mastodon" "https://toot.wales/@zosho") "."))))
         
  
 (make-page "about.html"
             (with-layout haunt-theme site "About" body)
             sxml->html))

					;
(define (now-page site posts)
  (define body
    
    `(section (@ (class "container"))
	      ((h2 "Now.")
	       (p "What I'm doing at the moment, inspired by Derek Sivers' " ,(anchor "Now" "https://sivers.org/nowff") " and which I heard of from ",(anchor "Patrick Rhone" "https://patrickrhone.com") ".")
	      (p "My current reading is " ,(current-reading) ".")
	      (p "My current project is " ,(current-project) ".")
	      (p "My current listening is " ,(current-listening) ".")
	      (hr)
	      (h3 Previously.)
	      (p "The last things I read were " ,(previous-reading) ".")

	      

	      )))
         
  
 (make-page "now.html"
             (with-layout haunt-theme site "Now" body)
             sxml->html))

(site #:title "somenotes"
      #:domain "somenotes.stevelloyd.net"
      #:default-metadata
      '((author . "Steve Lloyd")
        (email  . "steve@stevelloyd.net"))
      #:build-directory "public"
      #:readers (list commonmark-reader)
      #:builders (list (blog #:theme haunt-theme)
		       (flat-pages "pages"
				   #:template (theme-layout haunt-theme))

                       (atom-feed)
		       (rss-feed)
		       about-page
		       now-page
		       (static-directory "images")
		       (static-directory "css")))

