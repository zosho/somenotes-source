title: Now
---
## Now.


What I'm doing at the moment, inspired by Derek Sivers' [Now](https://sivers.org/nowff) and which I learned of from [Patrick Rhone](https://patrickrhone.com/).

My current reading is: 

- 'James', *Percival Everett*
- 'The Worlds I See: Curiosity, Exploration, and Discovery at the Dawn of AI', *Fei-Fei Li*

My current projects are:

- BBC Radio channels RSS update (BBC changed their page markup, so there's some XPath munging to do to make it parse the web pages correctly)
- beginning to work through 'The Little Learner, A Straight Line to Deep Learning', *Daniel Friedman and Anurag Mendhekar* 

My recent listening is *Journalist, Clive Myrie*, on 'Desert Island Discs'. Very moving and insightful. [Listen.](https://www.bbc.co.uk/sounds/play/m00208nv)

### Previously.

The last things I read were 'Dandelion Wine', *Ray Bradbury*, 'Tyger', *S F Said*, 'Fractals, on the edge of chaos' *Oliver Linton*, 'Kofi and the Rap Battle Summer, *Jeffrey Boakye*, 'God's Children Are Little Broken Things', *Arinze Ifeakandu*.

![A damp lane with green hedgerows. In the distance is a hill.](/images/tor-y-foel.jpg)

*Image: Tor y Foel from Llangynidr.*
