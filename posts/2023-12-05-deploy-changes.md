title: Site deployment changes
date: 2023-12-05 11:00
tags: haunt, gitlab
summary: Consolidated git repo for site
---

When this site was set up, I had separate build and deploy repositories. The deploy one had just the 'site' output from haunt which was then pushed to gitlab and served with gitlab pages. This was clearly a bit non-optimal, so now the sources and build config are now in the deploy repo also, so updating the site (resolution to make this more frequent pending). It needed the following steps:

- move the source files in, together with a simple go server which I use for stand-alone checks before making content live
- configure haunt to make the output build directory be *public* rather than the standard *site*. *haunt* has the ability to pass config into the build process in a file, but I recompiled the new location into the relevant spot in the source, key `build-directory`

```
(define* (site #:key
               (title "This Place is Haunted")
               (domain "example.com")
               (scheme 'https)
               (posts-directory "posts")
               (file-filter default-file-filter)
               (build-directory "public")
```

The outcome is that updates are a simple *write the markdown, check with the minimal server `go run server.go` and push to GitLab to deploy with GitLab Pages.

*minimal server for content checks*

```
package main

import (
    "log"
    "net/http"
    "fmt"
)

func main() {

     fmt.Println("site is displayed at http://localhost:8081")
     http.Handle("/", http.FileServer(http.Dir("./public")))

     log.Fatal(http.ListenAndServe(":8081", nil))

}

```

Anyway, no excuse for more posts. Also need to get to grips with the *haunt* source so be able to make some content helpers.

Here's some nice trees on a frosty day&#8230; 

![trees on the side of a frosty hill](images/trees.jpg)

