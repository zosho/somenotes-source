title: Raspberry Pi Pico environmental logger
date: 2023-07-06 18:00
tags: pico, applications
summary: Pico logger start of tinkering 
---

I've started a little environmental logger project, just for temperature, pressure and relative humidity. Using the Raspberry Pi Pico controller, a simple [BME280 sensor](https://shop.pimoroni.com/products/bme280-breakout?variant=29420960677971) and to provide a clock which won't lose time if the main power is disrupted, a STEMMA QT / Qwiic I2C interface to the Pico and  an SD card for data the Adafruit [PiCowbell](https://www.adafruit.com/product/5703). 

## Plan

Logger should eventually be able to run on battery for an extended time. This will mean exploring ways of reducing power consumption. Not an immediate goal.

The data can be accessed from a built-in web interface for quick checks on values - there won't be a local display.

Start off development by using the Pimoroni MicroPython library and runtime; this will allow used of the wifi capabilities.

Provide a git repository for any useful code which might come out of this.

To access the SD card and the clock will require using CircuitPython libraries - these can be used via the Adafruit [Blinka](https://learn.adafruit.com/circuitpython-libraries-on-micropython-using-the-raspberry-pi-pico) compatibility layer (hopefully).

For data presentation, there'll be a library for [Snap!](https://snap.berkeley.edu)

## Starting

I've got most of the components, and was just waiting for some headers which arrived today.

![Logger components](images/logger-components.jpg)

First task to solder headers onto the PiCowbell; so that it can be attached to the headers on the Pico, there's a procedure for this (on the Adafruit site) which involves soldering the four corner pins first to get the mechanical alignment right, then soldering the remainder. With the stacking, unfortunately the reset button is inaccessible, so a Pimoroni _Captain Resetti_ switch designed for the original Pico works well. Not an essential mod but handy for the first dev. Stages.

![Add reset switch](images/logger-reset.jpg)

Here's the final development package with the sensor connected - it ends up being quite compact.

![Development package](images/logger-development-setup.jpg)

## First code

Just to see that there was communication over I2C from the sensor, tried the below code. Some references for this are (PDF)

[Raspberry Pi MicroPython SDK](https://datasheets.raspberrypi.com/pico/raspberry-pi-pico-python-sdk.pdf)

… and the Pimoroni sensor libraries (we need the [BME280](https://github.com/pimoroni/pimoroni-pico/tree/main/micropython/modules/breakout_bme280) one)

First thing is to import the required libraries, then (blinking an LED is _Hello world_) set up the relevant pin (which changes for the PicoW from the standard Pico). An i2c port is setup and the reference to the device initialised. Finally a 1 second period timer is kicked off to grab and print out a reading and toggle the LED.

```
from machine import Pin, Timer
from breakout_bme280 import BreakoutBME280
from pimoroni_i2c import PimoroniI2C

led = Pin("LED", Pin.OUT)
PINS_BREAKOUT_GARDEN = {"sda": 4, "scl": 5}

i2c = PimoroniI2C(**PINS_BREAKOUT_GARDEN)
bme = BreakoutBME280(i2c)

tim = Timer()
def tick(timer):
    global led, bme
    led.toggle()
    reading = bme.read()
    print(reading[0], "C", reading[1], "Pa", reading[2], "%")

tim.init(freq=1, mode=Timer.PERIODIC, callback=tick)
```

![First readings](images/logger-first-readings.jpg)

Values are degrees C for temp, Pascals for pressure and a % for relative humidity. Looks like these are sensible.

Enough for today! 

   



