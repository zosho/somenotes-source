title: Weirding Apple—I'm all for it
date: 2024-02-16 11:00
tags: apple, iWeird
summary: Weirding Apple, &mdash; I'm all for it. Back to its roots.
---

There's been commentary eg from long-time writer-about-Apple Jason Snell [(read)](https://www.macworld.com/article/2235418), associated with the expensive, speculative step into the future evidenced by the Apple Vision Pro regarding a possible 'weirding' of Apple, and the great idea that would be.  But wait, Apple, isn't it that behemoth worth $3 trillion and cautious in the extreme? It was not always so, giving future hope. (I'm going to use a tag *iWeird* for this phenomenon henceforth.) Here's two examples of that iWeird spirit.

## One: the Newton (no Newton, no iPhone or Apple Vision Pro).

A common trope in the history of Apple is that of the Newton product, a handheld released hastily in 1993 with fledgling and sometimes hilariously-flawed handwriting recognition. But this was such a daring product which, arguably became among Apple's most significant. Heh? It's all in the forward-looking technology. To create Newton, Apple teamed up with Cambridge UK company Acorn Computers, to make use of [Sophie Wilson's](https://en.wikipedia.org/wiki/Sophie_Wilson) design for a low-power-consumption processor needed for it. This spawned the company which became ARM. You know, whose CPU  architecture is in all iPhones, iPads and Apple Silicon Macs. This experience kick-started Apple's building its own chip design team. So maybe it's not too far fetched to say, "no Newton, no Apple Silicon, *no iPhone or Apple Vision Pro* ". (Or at least maybe these products' momentum hindered for a decade.) &#8224; 

Don't get me started on Newton (oh, wait&mdash;I'm already going…). Its low power (hence good battery life) and possiblity of lower cost was ideal for educational products, such as the short-lived eMate 300 which was charming and fun to use. I'd love, as part of iWeird, for Apple to pursue less expensive learning devices for kids. More open (with I/O to allow tinkering with physical computing), maybe with gesture-sensing and spatial awareness to allow the sorts of interaction pioneered in Bret Victor's [Dynamicland](https://dynamicland.org). Maybe little boxes the size of an Apple TV.

![A translucent green laptop with monochrome screen.](images/Apple_Newton_eMate_300.jpg)

*Apple eMate 300. Image: Felix Winkelnkemper, CC BY-SA.* 

Another casualty of the Newton-era was its originally-planned software coming out of Apple's Eastern Research and Technology Group, based on the [Dylan](https://en.wikipedia.org/wiki/Dylan_(programming_language)) language. Again very innovative, and deriving from [Scheme](https://www.scheme.org)  (still for my money the best tool to explore ideas and turn them into something a person can understand and a computer can run with).

>  *After some analysis, we concluded that a language that would meet all our requirements would be more like Lisp than Smalltalk, except that, like Smalltalk, it would be "objects all the way down". By providing alternate syntaxes atop the extremely neutral base of Lisp, we believed we could present a familiar face not only to Lisp programmers, but also to those accustomed to other syntaxes…* &mdash;Larry Tesler

![Cover of a book with a pastel square-theme image. Titled Dylan, an object-oriented dynamic language](images/Dylan.jpg)

*Dylan language book.*

## Two: PostScript (no PostScript, no LaserWriter, no Apple)

In 1985 I started working for a small company in Oxford, UK [High Level Hardware](https://en.m.wikipedia.org/wiki/HLH_Orion), and borrowed a Mac from the office (the first time I'd had access to one) for doing documentation. They also had a [LaserWriter](https://en.m.wikipedia.org/wiki/LaserWriter) and I was just smitten with the ability to create content and see it printed with fidelity.

[PostScript](https://en.m.wikipedia.org/wiki/PostScript) was another iWeird excursion for Apple. Spotted by Steve Jobs on a visit to Adobe Systems to see its creators, the sadly now late John Warnock and Chuck Geschke. Its abilities to describe page structure in a language (similar to much beloved [Forth](https://en.wikipedia.org/wiki/Forth_(programming_language)) was just what was needed to embed in the LaserWriter, so kick-starting the desktop publishing industry, which was a major (maybe critical) boost to Apple's sales. Apple  may not have (barely) survived the 1990's without it.  

So, again weirdness (PostScript was a novel outlying technology when Apple adopted it) has been very important to the company's future.

There is always a danger that success and complacency will obscure an organization's appreciation of significant, risky milestones in getting to their exalted position. Maybe with the Apple Vision Pro, immediately successful or not we're seeing that some of this spirit can be harnessed.

iWeird is go! (Thunderbirds theme plays in the background…)

&#8224; I'm sorely tempted to say "*No Acorn BBC Micro, no iPhone*", but maybe that's stretching it 😂..




