title: Build process update
date: 2023-06-13 13:30
tags: haunt, guile
summary: I've updated the process to start building these notes to use 'Haunt'
---

I've made a first step to replace the build process for these notes as a precursor to updating them more often. This might seem to be mostly in the spirit of procrastination. Ah well, guilty as charged.

For a number of years, I've used the excellent static site builder [frog](https://github.com/greghendershott/frog) for work which I haven't ported to the current setup. This supports builders to compile a site from Markdown and Scribble and is very solid. I've been taking an interest in other possibilities, which might fit into the wishes:


    - should be written in a Lispy, preferably Scheme-like language (I should write something about why this choice)

    - a toolkit, rather than a monolithic one-size-fits-all thing

    - not so big that I'd be unable (despite applying some effort), to understand and adapt it in the future

    - markdown support

    - RSS and Atom support

Fortunately the range of candidates isn't so overwhelming, and [Haunt](https://dthompson.us/projects/haunt.html) seems to fit the bill nicely. It's written in [guile](https://www.gnu.org/software/guile/) which I've been aware of for ages, but not used much in earnest previously except for some scripting utilities for a work website. For those not familiar with the Scheme landscape, guile has a special place, being a commonly-chosen scripting language with a lineage back to the mid-1990s.

I spent a day (yesterday) setting up haunt, choosing a minimal CSS library [picocss](https://picocss.com) and reading/coopting the very useful introductory material and examples provided by the haunt author. Built it on Debian on a laptop (having run into some problems setting up on MacOS–to resolve on a future occasion). It might have been easier using guix package manager, but am not so confident of my skills with that on my eventual target build system (Debian-derived Raspbian). My deployment setup is a bit convoluted, but again another project for the future.

I'll need to work to understand the design principles of haunt (to be able to adapt it to my needs), but feeling enthused to be embarking on this task. 
   