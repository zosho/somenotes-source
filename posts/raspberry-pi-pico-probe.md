title: Raspberry Pi Pico Debug Probe
date: 2023-02-24 17:37
tags: pico
summary: Pico debug probe
---

Raspberry Pi have just released a little box which can be used to debug programs on their *Pico* microcontroller; for example to load code into the device, set breakpoints and examine the state of a fledgling program.

![Pico Debug Probe in packaging](images/Pico-Probe.jpg)


The pack includes a set of cables which provides:  a UART serial console (over the USB connection to the host), two options for a SWD (serial wire debug) connection over USB, JST connector built in to Pico H and WH, cable for the three-pin pads (which will need headers soldered on) for original Pico and Pico W.


A neat touch on the packaging is fold-up tabs picturing the options provided.

![Packaging tabs with connection options displayed](images/connection-options.jpg)

![Contents of the Pico Probe package](images/package-contents.jpg)

### Documentation

There's comprehensive documentation available at [https://www.raspberrypi.com/documentation/microcontrollers/debug-probe.html](https://www.raspberrypi.com/documentation/microcontrollers/debug-probe.html) including setting up the software needed to drive the probe (for different host platforms). If you want to wire it to the Pico/Pico W via the SWD pads, the wiring looks like:

![Pico/Pico W SWD pad connection to debug](images/SWD-pad-connection.jpg)

To summarise, this is a very neat, well-documented tool, which is itself implemented with the [RP2040 chip](https://www.raspberrypi.com/products/rp2040/), a demonstration of its place as an innovative and inexpensive embedded device.
