# somenotes-haunt

- [haunt](https://dthompson.us/projects/haunt.html)-based version of a blog sitenamed *somenotes* (until 2023 the site was built in Greg Hendershott's [Frog](https://docs.racket-lang.org/frog/index.html))
- this is the canonical repo, containing both the sources and (in the public directory) the published static content (the site is published using [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/))
- originally the 'plain pages' (*About* and *Now*) of the site were written using Scheme functions provided by haunt, now with the 0.3.0 haunt release these will be redone as Commonmark as per the posts. Also I swapped to using the standard `haunt serve` rather than originally a tiny go server for site checking
- the original Scheme functions for About and Now are still in the codebase for reference.

It's definitely a work in progress, but the clean design and facilities of haunt offer great scope for learning and development.
